﻿using Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Demo.Controllers
{
    public class ProductSearchController : Controller
    {
        // GET: ProductSearch
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetSearchList(string search)
        {
            ApplicationDbContext _db = new ApplicationDbContext();
            List<ProductModel> allsearch = _db.Products.Where(x => x.ProductName.Contains(search)).Select(x => new ProductModel
            {
                Id = x.ProductId,
                Name = x.ProductName
            }).ToList();
            return new JsonResult { Data = allsearch, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }


        public class ProductModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
    }
}